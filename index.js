/* eslint-disable no-console */
const logger = require('morgan');
const express = require('express');
const bodyParser = require('body-parser');
const dotenv = require('dotenv');
const cors = require('cors');
const https = require('https');

// Load .env from file if not production
if (process.env.NODE_ENV !== 'production') dotenv.config();

const weatherApi = process.env.OPENWEATHERMAP_API;
const weatherHost = process.env.OPENWEATHERMAP_HOST || 'api.openweathermap.org';
const weatherPort = process.env.OPENWEATHERMAP_PORT || 443;

if(!weatherApi)
    console.error('Invalid OpenWeatherMap API key!');

const options = {
    hostname: weatherHost,
    port: weatherPort,
    path: `/data/2.5/weather?APPID=${weatherApi}&units=metric`,
    method: 'GET'
};

const router = express.Router();
const routes = require('./api/routes')(router, https, options);

const app = express();
app.use(cors());
app.use(bodyParser.json());
app.use(logger('dev'));
// Router-level middleware
app.use('/api', routes);
// Error-handling middleware
// eslint-disable-next-line no-unused-vars
app.use((err, req, res, next) => {
    console.error(err.stack);
    res.status(500).send('Something broke!');
});

const port = process.env.PORT || 9999;

app.listen(port);
