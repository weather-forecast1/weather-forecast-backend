module.exports = (app, https, options, apiKey) => {
    app.get('/weather', async (req, res) => {
        try {
            options.path += '&q=ankara,tr';
            const httpRequest = https.request(options, httpResponse => {
                console.log(`statusCode: ${httpResponse.statusCode}`);
                httpResponse.on('data', d => {
                    let raw = JSON.parse(d);

                    if(!raw.weather || !raw.main || !raw.coord){
                        res.status(400).json({ message: 'Corrupted data' });
                        return;
                    }

                    let data = {};
                    let weather = raw.weather[0];
                    data.city = raw.name;
                    data.country = raw.sys.country;
                    data.pressure = raw.main.pressure;
                    data.temp = raw.main.temp;
                    data.timestamp = raw.dt;
                    data.iconURL = `http://openweathermap.org/img/wn/${weather.icon}@2x.png`;
                    data.iconDesc = weather.description;
                    data.humidity = raw.main.humidity;
                    data.wind = raw.wind.speed;
                    data.lat = raw.coord.lat;
                    data.lon = raw.coord.lon;
                    data.clouds = raw.clouds.all;
                    data.sunrise = raw.sys.sunrise;
                    data.sunset = raw.sys.sunset;

                    res.status(200).json(data);
                });
            });
        
            httpRequest.on('error', err => {
                res.status(400).json({ message: err.message });
            });
        
            httpRequest.end();
        } catch (err) {
            res.status(400).json({ message: err.message });
        }
    });
    
    app.get('/forecast', async (req, res) => {
    
    });

    return app;
};
